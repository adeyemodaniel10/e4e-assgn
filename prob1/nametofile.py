import time

def name_to_file(name):
    '''Write name to file with each character on separate line.
    
    Arguments:
    name -- type: str
    '''
    # lambda func appends the newline to each character in the name string 
    name_newline = map(lambda char: char+'\n', name)

    with open("result.txt", 'w') as f:
        f.writelines(name_newline)

if __name__ == '__main__':
    start = time.time()
    name_to_file("Daniel 'letroot' Adeyemo")
    print ("RUNTIME: {:.8f}s".format(time.time() - start))

    '''
    NOTE
    -----
    you can run this script by passing in your
    name as a command line argument as well
    and it would still work

    Usage: $> python nametofile.py Ezra Pound
    '''
    cmd_args = ''.join(sys.argv[1:])
    name_to_file(cmd_args)