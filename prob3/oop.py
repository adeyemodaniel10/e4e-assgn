class Adeyemo(object):
    pass

class Daniel(Adeyemo):

    def __init__(self):

        # super(Daniel)
        self.name = "Daniel Adeyemo"
        self.hobbies = ["-Reading", "-Watching foreign language films", "-Writing poetry"]

    def getHobies(self):
        """Return hobbies as string."""

        return '\n'.join(self.hobbies)

    def __str__(self):
        return "My name is {}.\nMy hobbies are:\n{}".format(self.name, self.getHobies())

if __name__ == '__main__':
    dan = Daniel()
    print(dan.getHobies())
    print("\n")
    print(dan)