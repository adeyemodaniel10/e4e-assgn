from django.db import models
from django.conf import settings

class Post(models.Model):
    # userId    = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title   = models.CharField(max_length=140, null=True, blank=True)
    body    = models.CharField(max_length=280, null=True, blank=True)

    def __str__(self):
        return str(self.title)