from rest_framework import generics, mixins
from posts.models import Post
from .serializers import PostSerializer

class PostAPIView(mixins.CreateModelMixin, generics.ListAPIView):
    lookup_field     = 'pk'
    serializer_class = PostSerializer
    queryset         = Post.objects.all()

    def perform_create(self, serializer):
        serializer.save()

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get_serializer_context(self, *args, **kwargs):
        return {"request": self.request}

class PostRudView(generics.RetrieveUpdateDestroyAPIView):
    lookup_field     = 'pk'
    serializer_class = PostSerializer

    def get_queryset(self):
        return Post.objects.all()

    def get_serializer_context(self, *args, **kwargs):
        return {"request": self.request}

    def get_object(self):
        pk = self.kwargs.get("pk")
        return Post.objects.get(pk=pk)