from posts.models import Post

from rest_framework import serializers

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = [
            'id',
            # 'userId',
            'title',
            'body',
        ]
        read_only_fields = ['id',]

    def validate_title(self, value):
        if value == '':
            raise serializers.ValidationError("Title cannot be empty")
        return value

    def validate_body(self, value):
        if value == '':
            raise serializers.ValidationError("Body cannot be empty")
        return value