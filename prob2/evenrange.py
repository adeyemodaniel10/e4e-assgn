def even_num_lst(start=0, end=10):
    """Return a list of even numbers between two numbers
        not including those numbers.
    
    Keyword arguments:
    start -- <type: int> specifying the lower bound (default 0)
    end -- <type: int> specifying the upper bound (default 10)
    start should be less than or equal to end
    """

    # this conditional guards the situation where start is odd
    if (start % 2) != 0:
        start += 1
    else: start += 2

    return list(range(start, end, 2))

def test():
    assert even_num_lst(-1, -9) == []
    assert even_num_lst(2,9) == [4,6,8]
    assert even_num_lst(1,8) == [2,4,6]
    assert even_num_lst(1,9) == [2,4,6,8]
    assert even_num_lst(0,0) == []
    assert even_num_lst(0,2) == []
    assert even_num_lst(8,2) == []
    assert even_num_lst(-9, -1) == [-8, -6, -4, -2]

if __name__ == '__main__':
    test()   # run tests
    print(even_num_lst(3,8))
    print(even_num_lst())
